#!/bin/bash

NAMESPACE=$1
VAR_FILE=$2
# For Helm 3
kubectl create ns $NAMESPACE
helm install $NAMESPACE-runner  gitlab/gitlab-runner --namespace $NAMESPACE -f $VAR_FILE
