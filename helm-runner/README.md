# Helm Deploy

Scripts and charts for deploying the GitLab Runner to the created. Note, to deploy helm you will need: 

1. Kubectl access to the cluster
1. Helm installed on the system
1. Helm repo downloaded from [GitLab](https://docs.gitlab.com/runner/install/kubernetes.html#installing-gitlab-runner-using-the-helm-chart)
1. Deploy example toml and helm chart with script in this repo (add your own key).

